#include <unistd.h>
#include <stdio.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <stdlib.h>
#include <byteswap.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#define eprintf(s, ...) fprintf(stderr, s, ##__VA_ARGS__)

// Get system info then print it.
int print_info() {
  struct sysinfo s_info;
  int err = sysinfo(&s_info);
  if(err != 0) return err;

  // Output in json form
  printf("{\n");
  printf( "\t\"totalram\": %lu,\n", s_info.totalram);
  printf(  "\t\"freeram\": %lu,\n", s_info.freeram);
  printf("\t\"sharedram\": %lu,\n", s_info.sharedram);
  printf("\t\"bufferram\": %lu,\n", s_info.bufferram);
  printf("\t\"totalswap\": %lu,\n", s_info.totalswap);
  printf("\t\"freeswap\": %lu,\n", s_info.freeswap);
  printf("\t\"procs\": %hu,\n", s_info.procs);
  printf("\t\"totalhigh\": %lu,\n", s_info.totalhigh);
  printf("\t\"freehigh\": %lu,\n", s_info.freehigh);
  printf("\t\"mem_unit\": %u\n", s_info.mem_unit);
  printf("}\n");

  return 0;
}

/* Connect to a uint32_t ip and port and return the socket
 * returns -1 on error and sets errno.
 * on an error condition the socket is closed internally.
 */
int connect_to(uint32_t ip, uint16_t port)  {
  struct sockaddr_in sa;
  sa.sin_family = AF_INET;
  sa.sin_addr.s_addr = ip;
  sa.sin_port = htons(port);

  int s = socket(AF_INET, SOCK_STREAM, 0);
  if (connect(s, (struct sockaddr *)&sa, sizeof(sa)) != 0) {
    close(s);
    return -1;
  }

  return s;
}

// TODO: understand this, copied from stackoverflow
uint32_t deserialize_uint32(unsigned char *buffer) {
  uint32_t value = 0;
  value |= buffer[0] << 24;
  value |= buffer[1] << 16;
  value |= buffer[2] << 8;
  value |= buffer[3];
  return value;
}

int main(int argc, char const* argv[])
{
  if (argc != 3) {
    eprintf("Usage: %s <ip> <port>\n", argv[0]);
    return 1;
  }

  const char* rhost = argv[1];
  const int rport = atoi(argv[2]);
  if (rport == 0) {
    eprintf("Invalid port '%s'", argv[2]);
    return 1;
  }

  struct hostent *he = gethostbyname(rhost);
  if (!he) {
    eprintf("Lookup Failed: %s\n", strerror(errno));
    return 1;
  }

  int conn = -1;

  eprintf("Resolved %s to %s\n", rhost, he->h_name);
  for (int i = 0; he->h_addr_list[i] != NULL; i++) {
    eprintf("Connecting to: %s... ", inet_ntoa((struct in_addr) * ((struct in_addr *) he->h_addr_list[i])));

    uint32_t ip = deserialize_uint32((unsigned char*)he->h_addr_list[i]);
    ip = __bswap_32(ip);

    conn = connect_to(ip, rport);
    if (conn == -1) {
      eprintf("Failed: %s\n", strerror(errno));
    } else {
      eprintf("Done\n");
      break;
    }
  }

  // todo: print error message?
  if (conn == -1) return 1;

  dup2(conn, 0);
  dup2(conn, 1);
  dup2(conn, 2);

  if (print_info() != 0) {
    eprintf("Failed to print info: %s\n", strerror(errno));
    return -1;
  }
  close(conn);
  return 0;
}
