#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const* argv[])
{
  if (argc != 2) return 1;

  printf("%d\n", atoi(argv[1]));
  return 0;
}
