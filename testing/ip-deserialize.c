#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>

int main(int argc, char const* argv[])
{
  if (argc != 2) {
    printf("Usage: %s <ip>\n", argv[0]);
    return 1;
  }

  struct in_addr addr;

  if (inet_aton(argv[1], &addr) == 0) {
    fprintf(stderr, "Invalid address\n");
    return 1;
  }

  printf("%d\n", addr.s_addr);
  printf("%s\n", inet_ntoa(addr));

  return 0;
}
